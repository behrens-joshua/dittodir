﻿using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("DittoDir GUI WinForms")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("")]
[assembly: AssemblyProduct("DittoDir")]
[assembly: AssemblyCopyright("Copyright © Thorsten Gerds, Joshua Behrens 2016")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

[assembly: ComVisible(false)]
[assembly: Guid("a8c75ddc-5f01-4795-b5f0-8ea1fd7e178f")]
[assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyFileVersion("1.0")]
