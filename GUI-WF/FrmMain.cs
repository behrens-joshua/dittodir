﻿namespace DittoDir.GUI.WinForms
{
    using System.Windows.Forms;

    public partial class FrmMain : Form
    {
        public FrmMain()
        {
            InitializeComponent();
            sfdMetadataFile.InitialDirectory = System.Environment.GetFolderPath(System.Environment.SpecialFolder.MyComputer);
            sfdLogFile.InitialDirectory = System.Environment.GetFolderPath(System.Environment.SpecialFolder.MyComputer);
        }

        private void btnSetRules_Click(object sender, System.EventArgs e)
        {
            DlgCopyRules copyRules = new DlgCopyRules();
            copyRules.ShowDialog(this);
        }

        private void btnSelectInputDirectory_Click(object sender, System.EventArgs e)
        {
            if (fbdInput.ShowDialog(this) == DialogResult.OK)
            {
                tbInputDirectory.Text = fbdInput.SelectedPath;
            }
        }

        private void btnSelectOutputDirectory_Click(object sender, System.EventArgs e)
        {
            if (fbdOutput.ShowDialog(this) == DialogResult.OK)
            {
                tbOutputDirectory.Text = fbdOutput.SelectedPath;
            }
        }

        private void btnSelectMetadataFile_Click(object sender, System.EventArgs e)
        {
            sfdMetadataFile.ShowDialog(this);
        }

        private void btnSelectLogFile_Click(object sender, System.EventArgs e)
        {
            sfdLogFile.ShowDialog(this);
        }

        private void sfdMetadataFile_FileOk(object sender, System.ComponentModel.CancelEventArgs e)
        {
            tbMetadataFile.Text = sfdMetadataFile.FileName;
        }

        private void sfdLogFile_FileOk(object sender, System.ComponentModel.CancelEventArgs e)
        {
            tbLogFile.Text = sfdLogFile.FileName;
        }
    }
}
