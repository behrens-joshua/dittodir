﻿namespace DittoDir.GUI.WinForms
{
    partial class DlgCopyRules
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.ListView lvRules;
            System.Windows.Forms.TableLayoutPanel pnlLayout;
            System.Windows.Forms.Button btnAdd;
            System.Windows.Forms.Button btnRemove;
            System.Windows.Forms.Button btnMoveUp;
            System.Windows.Forms.Button btnMoveDown;
            System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
            System.Windows.Forms.Button btnClose;
            this.colSelector = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colCopyingActionType = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            lvRules = new System.Windows.Forms.ListView();
            pnlLayout = new System.Windows.Forms.TableLayoutPanel();
            btnAdd = new System.Windows.Forms.Button();
            btnRemove = new System.Windows.Forms.Button();
            btnMoveUp = new System.Windows.Forms.Button();
            btnMoveDown = new System.Windows.Forms.Button();
            flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            btnClose = new System.Windows.Forms.Button();
            pnlLayout.SuspendLayout();
            flowLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // lvRules
            // 
            lvRules.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            lvRules.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colSelector,
            this.colCopyingActionType});
            lvRules.Location = new System.Drawing.Point(3, 3);
            lvRules.Name = "lvRules";
            pnlLayout.SetRowSpan(lvRules, 5);
            lvRules.Size = new System.Drawing.Size(379, 286);
            lvRules.TabIndex = 0;
            lvRules.UseCompatibleStateImageBehavior = false;
            lvRules.View = System.Windows.Forms.View.Details;
            // 
            // colSelector
            // 
            this.colSelector.Text = "Filetype selector";
            this.colSelector.Width = 200;
            // 
            // colCopyingActionType
            // 
            this.colCopyingActionType.Text = "Copying action";
            this.colCopyingActionType.Width = 175;
            // 
            // pnlLayout
            // 
            pnlLayout.ColumnCount = 2;
            pnlLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            pnlLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            pnlLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            pnlLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            pnlLayout.Controls.Add(lvRules, 0, 0);
            pnlLayout.Controls.Add(btnAdd, 1, 0);
            pnlLayout.Controls.Add(btnRemove, 1, 1);
            pnlLayout.Controls.Add(btnMoveUp, 1, 2);
            pnlLayout.Controls.Add(btnMoveDown, 1, 3);
            pnlLayout.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlLayout.Location = new System.Drawing.Point(0, 0);
            pnlLayout.Name = "pnlLayout";
            pnlLayout.RowCount = 5;
            pnlLayout.RowStyles.Add(new System.Windows.Forms.RowStyle());
            pnlLayout.RowStyles.Add(new System.Windows.Forms.RowStyle());
            pnlLayout.RowStyles.Add(new System.Windows.Forms.RowStyle());
            pnlLayout.RowStyles.Add(new System.Windows.Forms.RowStyle());
            pnlLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            pnlLayout.Size = new System.Drawing.Size(464, 292);
            pnlLayout.TabIndex = 0;
            // 
            // btnAdd
            // 
            btnAdd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            btnAdd.AutoSize = true;
            btnAdd.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            btnAdd.Location = new System.Drawing.Point(388, 3);
            btnAdd.Name = "btnAdd";
            btnAdd.Size = new System.Drawing.Size(73, 23);
            btnAdd.TabIndex = 2;
            btnAdd.Text = "Add";
            btnAdd.UseVisualStyleBackColor = true;
            // 
            // btnRemove
            // 
            btnRemove.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            btnRemove.AutoSize = true;
            btnRemove.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            btnRemove.Location = new System.Drawing.Point(388, 32);
            btnRemove.Name = "btnRemove";
            btnRemove.Size = new System.Drawing.Size(73, 23);
            btnRemove.TabIndex = 3;
            btnRemove.Text = "Remove";
            btnRemove.UseVisualStyleBackColor = true;
            // 
            // btnMoveUp
            // 
            btnMoveUp.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            btnMoveUp.AutoSize = true;
            btnMoveUp.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            btnMoveUp.Location = new System.Drawing.Point(388, 61);
            btnMoveUp.Name = "btnMoveUp";
            btnMoveUp.Size = new System.Drawing.Size(73, 23);
            btnMoveUp.TabIndex = 4;
            btnMoveUp.Text = "Move up";
            btnMoveUp.UseVisualStyleBackColor = true;
            // 
            // btnMoveDown
            // 
            btnMoveDown.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            btnMoveDown.AutoSize = true;
            btnMoveDown.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            btnMoveDown.Location = new System.Drawing.Point(388, 90);
            btnMoveDown.Name = "btnMoveDown";
            btnMoveDown.Size = new System.Drawing.Size(73, 23);
            btnMoveDown.TabIndex = 5;
            btnMoveDown.Text = "Move down";
            btnMoveDown.UseVisualStyleBackColor = true;
            // 
            // flowLayoutPanel1
            // 
            flowLayoutPanel1.AutoSize = true;
            flowLayoutPanel1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            flowLayoutPanel1.Controls.Add(btnClose);
            flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            flowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft;
            flowLayoutPanel1.Location = new System.Drawing.Point(0, 292);
            flowLayoutPanel1.Margin = new System.Windows.Forms.Padding(0);
            flowLayoutPanel1.Name = "flowLayoutPanel1";
            flowLayoutPanel1.Size = new System.Drawing.Size(464, 29);
            flowLayoutPanel1.TabIndex = 1;
            // 
            // btnClose
            // 
            btnClose.Anchor = System.Windows.Forms.AnchorStyles.Right;
            btnClose.AutoSize = true;
            btnClose.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            btnClose.DialogResult = System.Windows.Forms.DialogResult.OK;
            btnClose.Location = new System.Drawing.Point(386, 3);
            btnClose.MinimumSize = new System.Drawing.Size(75, 0);
            btnClose.Name = "btnClose";
            btnClose.Size = new System.Drawing.Size(75, 23);
            btnClose.TabIndex = 0;
            btnClose.Text = "Close";
            btnClose.UseVisualStyleBackColor = true;
            // 
            // DlgCopyRules
            // 
            this.AcceptButton = btnClose;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(464, 321);
            this.Controls.Add(pnlLayout);
            this.Controls.Add(flowLayoutPanel1);
            this.Name = "DlgCopyRules";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Copy rules";
            pnlLayout.ResumeLayout(false);
            pnlLayout.PerformLayout();
            flowLayoutPanel1.ResumeLayout(false);
            flowLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ColumnHeader colSelector;
        private System.Windows.Forms.ColumnHeader colCopyingActionType;
    }
}