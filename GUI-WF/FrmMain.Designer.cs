﻿namespace DittoDir.GUI.WinForms
{
    partial class FrmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.TableLayoutPanel pnlLayout1;
            System.Windows.Forms.Button btnStart;
            System.Windows.Forms.Label lblInputDirectory;
            System.Windows.Forms.Button btnSetRules;
            System.Windows.Forms.Label lblOutputDirectory;
            System.Windows.Forms.Label lblMetadataFile;
            System.Windows.Forms.Label lblLogFile;
            System.Windows.Forms.RichTextBox tbLog;
            System.Windows.Forms.Button btnSelectInputDirectory;
            System.Windows.Forms.Button btnSelectOutputDirectory;
            System.Windows.Forms.Button btnSelectMetadataFile;
            System.Windows.Forms.Button btnSelectLogFile;
            System.Windows.Forms.Label lblLiveLog;
            this.tbInputDirectory = new System.Windows.Forms.TextBox();
            this.tbOutputDirectory = new System.Windows.Forms.TextBox();
            this.tbMetadataFile = new System.Windows.Forms.TextBox();
            this.tbLogFile = new System.Windows.Forms.TextBox();
            this.fbdInput = new System.Windows.Forms.FolderBrowserDialog();
            this.fbdOutput = new System.Windows.Forms.FolderBrowserDialog();
            this.sfdMetadataFile = new System.Windows.Forms.SaveFileDialog();
            this.sfdLogFile = new System.Windows.Forms.SaveFileDialog();
            pnlLayout1 = new System.Windows.Forms.TableLayoutPanel();
            btnStart = new System.Windows.Forms.Button();
            lblInputDirectory = new System.Windows.Forms.Label();
            btnSetRules = new System.Windows.Forms.Button();
            lblOutputDirectory = new System.Windows.Forms.Label();
            lblMetadataFile = new System.Windows.Forms.Label();
            lblLogFile = new System.Windows.Forms.Label();
            tbLog = new System.Windows.Forms.RichTextBox();
            btnSelectInputDirectory = new System.Windows.Forms.Button();
            btnSelectOutputDirectory = new System.Windows.Forms.Button();
            btnSelectMetadataFile = new System.Windows.Forms.Button();
            btnSelectLogFile = new System.Windows.Forms.Button();
            lblLiveLog = new System.Windows.Forms.Label();
            pnlLayout1.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlLayout1
            // 
            pnlLayout1.ColumnCount = 3;
            pnlLayout1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            pnlLayout1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            pnlLayout1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            pnlLayout1.Controls.Add(btnStart, 1, 6);
            pnlLayout1.Controls.Add(lblInputDirectory, 0, 0);
            pnlLayout1.Controls.Add(btnSetRules, 1, 4);
            pnlLayout1.Controls.Add(lblOutputDirectory, 0, 1);
            pnlLayout1.Controls.Add(lblMetadataFile, 0, 2);
            pnlLayout1.Controls.Add(lblLogFile, 0, 3);
            pnlLayout1.Controls.Add(this.tbInputDirectory, 1, 0);
            pnlLayout1.Controls.Add(this.tbOutputDirectory, 1, 1);
            pnlLayout1.Controls.Add(this.tbMetadataFile, 1, 2);
            pnlLayout1.Controls.Add(this.tbLogFile, 1, 3);
            pnlLayout1.Controls.Add(tbLog, 0, 5);
            pnlLayout1.Controls.Add(btnSelectInputDirectory, 2, 0);
            pnlLayout1.Controls.Add(btnSelectOutputDirectory, 2, 1);
            pnlLayout1.Controls.Add(btnSelectMetadataFile, 2, 2);
            pnlLayout1.Controls.Add(btnSelectLogFile, 2, 3);
            pnlLayout1.Controls.Add(lblLiveLog, 0, 4);
            pnlLayout1.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlLayout1.Location = new System.Drawing.Point(0, 0);
            pnlLayout1.Name = "pnlLayout1";
            pnlLayout1.RowCount = 7;
            pnlLayout1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            pnlLayout1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            pnlLayout1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            pnlLayout1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            pnlLayout1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            pnlLayout1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            pnlLayout1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            pnlLayout1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            pnlLayout1.Size = new System.Drawing.Size(624, 441);
            pnlLayout1.TabIndex = 0;
            // 
            // btnStart
            // 
            btnStart.Anchor = System.Windows.Forms.AnchorStyles.Right;
            btnStart.AutoSize = true;
            btnStart.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            pnlLayout1.SetColumnSpan(btnStart, 2);
            btnStart.Location = new System.Drawing.Point(546, 415);
            btnStart.MinimumSize = new System.Drawing.Size(75, 0);
            btnStart.Name = "btnStart";
            btnStart.Size = new System.Drawing.Size(75, 23);
            btnStart.TabIndex = 15;
            btnStart.Text = "Start";
            btnStart.UseVisualStyleBackColor = false;
            // 
            // lblInputDirectory
            // 
            lblInputDirectory.Anchor = System.Windows.Forms.AnchorStyles.Left;
            lblInputDirectory.AutoSize = true;
            lblInputDirectory.Location = new System.Drawing.Point(3, 8);
            lblInputDirectory.Margin = new System.Windows.Forms.Padding(3);
            lblInputDirectory.Name = "lblInputDirectory";
            lblInputDirectory.Size = new System.Drawing.Size(77, 13);
            lblInputDirectory.TabIndex = 0;
            lblInputDirectory.Text = "Input directory:";
            lblInputDirectory.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnSetRules
            // 
            btnSetRules.Anchor = System.Windows.Forms.AnchorStyles.Right;
            btnSetRules.AutoSize = true;
            btnSetRules.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            pnlLayout1.SetColumnSpan(btnSetRules, 2);
            btnSetRules.Location = new System.Drawing.Point(546, 119);
            btnSetRules.MinimumSize = new System.Drawing.Size(75, 0);
            btnSetRules.Name = "btnSetRules";
            btnSetRules.Size = new System.Drawing.Size(75, 23);
            btnSetRules.TabIndex = 12;
            btnSetRules.Text = "Copy rules";
            btnSetRules.UseVisualStyleBackColor = true;
            btnSetRules.Click += new System.EventHandler(this.btnSetRules_Click);
            // 
            // lblOutputDirectory
            // 
            lblOutputDirectory.Anchor = System.Windows.Forms.AnchorStyles.Left;
            lblOutputDirectory.AutoSize = true;
            lblOutputDirectory.Location = new System.Drawing.Point(3, 37);
            lblOutputDirectory.Margin = new System.Windows.Forms.Padding(3);
            lblOutputDirectory.Name = "lblOutputDirectory";
            lblOutputDirectory.Size = new System.Drawing.Size(85, 13);
            lblOutputDirectory.TabIndex = 3;
            lblOutputDirectory.Text = "Output directory:";
            lblOutputDirectory.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblMetadataFile
            // 
            lblMetadataFile.Anchor = System.Windows.Forms.AnchorStyles.Left;
            lblMetadataFile.AutoSize = true;
            lblMetadataFile.Location = new System.Drawing.Point(3, 66);
            lblMetadataFile.Margin = new System.Windows.Forms.Padding(3);
            lblMetadataFile.Name = "lblMetadataFile";
            lblMetadataFile.Size = new System.Drawing.Size(71, 13);
            lblMetadataFile.TabIndex = 6;
            lblMetadataFile.Text = "Metadata file:";
            lblMetadataFile.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblLogFile
            // 
            lblLogFile.Anchor = System.Windows.Forms.AnchorStyles.Left;
            lblLogFile.AutoSize = true;
            lblLogFile.Location = new System.Drawing.Point(3, 95);
            lblLogFile.Margin = new System.Windows.Forms.Padding(3);
            lblLogFile.Name = "lblLogFile";
            lblLogFile.Size = new System.Drawing.Size(44, 13);
            lblLogFile.TabIndex = 9;
            lblLogFile.Text = "Log file:";
            lblLogFile.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tbInputDirectory
            // 
            this.tbInputDirectory.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.tbInputDirectory.Location = new System.Drawing.Point(94, 4);
            this.tbInputDirectory.Name = "tbInputDirectory";
            this.tbInputDirectory.Size = new System.Drawing.Size(495, 20);
            this.tbInputDirectory.TabIndex = 1;
            // 
            // tbOutputDirectory
            // 
            this.tbOutputDirectory.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.tbOutputDirectory.Location = new System.Drawing.Point(94, 33);
            this.tbOutputDirectory.Name = "tbOutputDirectory";
            this.tbOutputDirectory.Size = new System.Drawing.Size(495, 20);
            this.tbOutputDirectory.TabIndex = 4;
            // 
            // tbMetadataFile
            // 
            this.tbMetadataFile.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.tbMetadataFile.Location = new System.Drawing.Point(94, 62);
            this.tbMetadataFile.Name = "tbMetadataFile";
            this.tbMetadataFile.Size = new System.Drawing.Size(495, 20);
            this.tbMetadataFile.TabIndex = 7;
            // 
            // tbLogFile
            // 
            this.tbLogFile.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.tbLogFile.Location = new System.Drawing.Point(94, 91);
            this.tbLogFile.Name = "tbLogFile";
            this.tbLogFile.Size = new System.Drawing.Size(495, 20);
            this.tbLogFile.TabIndex = 10;
            // 
            // tbLog
            // 
            tbLog.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            pnlLayout1.SetColumnSpan(tbLog, 3);
            tbLog.Location = new System.Drawing.Point(3, 148);
            tbLog.Name = "tbLog";
            tbLog.Size = new System.Drawing.Size(618, 261);
            tbLog.TabIndex = 14;
            tbLog.Text = "";
            // 
            // btnSelectInputDirectory
            // 
            btnSelectInputDirectory.Anchor = System.Windows.Forms.AnchorStyles.None;
            btnSelectInputDirectory.AutoSize = true;
            btnSelectInputDirectory.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            btnSelectInputDirectory.Location = new System.Drawing.Point(595, 3);
            btnSelectInputDirectory.Name = "btnSelectInputDirectory";
            btnSelectInputDirectory.Size = new System.Drawing.Size(26, 23);
            btnSelectInputDirectory.TabIndex = 2;
            btnSelectInputDirectory.Text = "...";
            btnSelectInputDirectory.UseVisualStyleBackColor = true;
            btnSelectInputDirectory.Click += new System.EventHandler(this.btnSelectInputDirectory_Click);
            // 
            // btnSelectOutputDirectory
            // 
            btnSelectOutputDirectory.Anchor = System.Windows.Forms.AnchorStyles.None;
            btnSelectOutputDirectory.AutoSize = true;
            btnSelectOutputDirectory.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            btnSelectOutputDirectory.Location = new System.Drawing.Point(595, 32);
            btnSelectOutputDirectory.Name = "btnSelectOutputDirectory";
            btnSelectOutputDirectory.Size = new System.Drawing.Size(26, 23);
            btnSelectOutputDirectory.TabIndex = 5;
            btnSelectOutputDirectory.Text = "...";
            btnSelectOutputDirectory.UseVisualStyleBackColor = true;
            btnSelectOutputDirectory.Click += new System.EventHandler(this.btnSelectOutputDirectory_Click);
            // 
            // btnSelectMetadataFile
            // 
            btnSelectMetadataFile.Anchor = System.Windows.Forms.AnchorStyles.None;
            btnSelectMetadataFile.AutoSize = true;
            btnSelectMetadataFile.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            btnSelectMetadataFile.Location = new System.Drawing.Point(595, 61);
            btnSelectMetadataFile.Name = "btnSelectMetadataFile";
            btnSelectMetadataFile.Size = new System.Drawing.Size(26, 23);
            btnSelectMetadataFile.TabIndex = 8;
            btnSelectMetadataFile.Text = "...";
            btnSelectMetadataFile.UseVisualStyleBackColor = true;
            btnSelectMetadataFile.Click += new System.EventHandler(this.btnSelectMetadataFile_Click);
            // 
            // btnSelectLogFile
            // 
            btnSelectLogFile.Anchor = System.Windows.Forms.AnchorStyles.None;
            btnSelectLogFile.AutoSize = true;
            btnSelectLogFile.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            btnSelectLogFile.Location = new System.Drawing.Point(595, 90);
            btnSelectLogFile.Name = "btnSelectLogFile";
            btnSelectLogFile.Size = new System.Drawing.Size(26, 23);
            btnSelectLogFile.TabIndex = 11;
            btnSelectLogFile.Text = "...";
            btnSelectLogFile.UseVisualStyleBackColor = true;
            btnSelectLogFile.Click += new System.EventHandler(this.btnSelectLogFile_Click);
            // 
            // lblLiveLog
            // 
            lblLiveLog.Anchor = System.Windows.Forms.AnchorStyles.Left;
            lblLiveLog.AutoSize = true;
            lblLiveLog.Location = new System.Drawing.Point(3, 124);
            lblLiveLog.Margin = new System.Windows.Forms.Padding(3);
            lblLiveLog.Name = "lblLiveLog";
            lblLiveLog.Size = new System.Drawing.Size(47, 13);
            lblLiveLog.TabIndex = 13;
            lblLiveLog.Text = "Live log:";
            lblLiveLog.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // fbdInput
            // 
            this.fbdInput.RootFolder = System.Environment.SpecialFolder.MyComputer;
            this.fbdInput.ShowNewFolderButton = false;
            // 
            // fbdOutput
            // 
            this.fbdOutput.RootFolder = System.Environment.SpecialFolder.MyComputer;
            // 
            // sfdMetadataFile
            // 
            this.sfdMetadataFile.DefaultExt = "ddmd";
            this.sfdMetadataFile.Filter = "DittoDir metadata|*.ddmd";
            this.sfdMetadataFile.Title = "Choose a destination for the DittoDir metadata file";
            this.sfdMetadataFile.FileOk += new System.ComponentModel.CancelEventHandler(this.sfdMetadataFile_FileOk);
            // 
            // sfdLogFile
            // 
            this.sfdLogFile.DefaultExt = "csv";
            this.sfdLogFile.Filter = "DittoDir logs|*.csv";
            this.sfdLogFile.Title = "Choose a destination for the DittoDir log file";
            this.sfdLogFile.FileOk += new System.ComponentModel.CancelEventHandler(this.sfdLogFile_FileOk);
            // 
            // FrmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(624, 441);
            this.Controls.Add(pnlLayout1);
            this.Name = "FrmMain";
            this.Text = "DittoDir";
            pnlLayout1.ResumeLayout(false);
            pnlLayout1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.FolderBrowserDialog fbdInput;
        private System.Windows.Forms.FolderBrowserDialog fbdOutput;
        private System.Windows.Forms.SaveFileDialog sfdMetadataFile;
        private System.Windows.Forms.SaveFileDialog sfdLogFile;
        private System.Windows.Forms.TextBox tbInputDirectory;
        private System.Windows.Forms.TextBox tbOutputDirectory;
        private System.Windows.Forms.TextBox tbMetadataFile;
        private System.Windows.Forms.TextBox tbLogFile;
    }
}