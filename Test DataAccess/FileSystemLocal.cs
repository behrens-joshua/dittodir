﻿namespace DittoDir.DataAccess.Test
{
    using System;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Diagnostics;
    using FileSystem;

    [TestClass]
    public class FileSystemLocal
    {
        private readonly IDictionary<string, string> FileList_txt = new Dictionary<string, string>();

        [TestInitialize]
        public void LoadFileList()
        {
            FileList_txt.Clear();

            foreach (string line in File.ReadAllLines(Path.GetFullPath("./FileSystemLocal/FileList.txt")).Where(p => p.Trim().Length > 0))
            {
                Debug.Assert(line.Count(p => p == ':') == 1);

                string[] info = line.Split(":".ToCharArray(), StringSplitOptions.None);

                FileList_txt.Add(new KeyValuePair<string, string>(info[0], info[1]));
            }
        }

        [TestMethod]
        public void DirectoryListing()
        {
            IAmFileSystemObject fso = FileSystemResolver.ReadUri(new Uri(Path.GetFullPath("./FileSystemLocal/")));

            Stack<IAmFileSystemObject> todo = new Stack<IAmFileSystemObject>();
            todo.Push(fso);
            
            while (todo.Any())
            {
                fso = todo.Pop();

                if (fso is IAmDirectory)
                {
                    foreach (var child in (fso as IAmDirectory).Children)
                    {
                        todo.Push(child);
                    }

                    Assert.IsTrue(FileList_txt.ContainsKey(fso.RelativePath));
                }
                else if (fso is IAmFile)
                {
                    string key = fso.RelativePath;

                    if (FileList_txt.ContainsKey(key) && !string.IsNullOrWhiteSpace(FileList_txt[key]))
                    {
                        Assert.AreEqual(FileList_txt[key].ToLower(), (fso as IAmFile).CalculateChecksum().Checksum.ToLower());
                    }
                }
            }
        }


        [TestMethod]
        public void DirectoryListingTypeCheck()
        {
            IAmFileSystemObject fso = FileSystemResolver.ReadUri(new Uri(Path.GetFullPath("./FileSystemLocal/")));

            Stack<IAmFileSystemObject> todo = new Stack<IAmFileSystemObject>();
            todo.Push(fso);

            Assert.IsInstanceOfType(fso, typeof(IAmDirectory));

            while (todo.Any())
            {
                fso = todo.Pop();

                if (fso is IAmDirectory)
                {
                    Assert.AreEqual(fso.GetType().FullName, "DittoDir.DataAccess.FileSystem.Local.Directory");

                    foreach (var child in (fso as IAmDirectory).Children)
                    {
                        todo.Push(child);
                    }
                }
                else if (fso is IAmFile)
                {
                    Assert.AreEqual(fso.GetType().FullName, "DittoDir.DataAccess.FileSystem.Local.File");

                    string key = fso.RelativePath;

                    if (FileList_txt.ContainsKey(key))
                    {
                        Assert.IsInstanceOfType((fso as IAmFile).CalculateChecksum(), typeof(Cryptography.MD5.Checksum));
                    }
                }
                else
                {
                    Assert.Fail("Unexpected type: " + fso.GetType().FullName);
                }
            }
        }
    }
}
