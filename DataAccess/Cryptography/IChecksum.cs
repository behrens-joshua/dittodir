﻿namespace DittoDir.DataAccess.Cryptography
{
    /// <summary>
    /// Holds a checksum.
    /// </summary>
    public interface IChecksum
    {
        /// <summary>
        /// Gets the checksum.
        /// </summary>
        string Checksum { get; }
    }
}
