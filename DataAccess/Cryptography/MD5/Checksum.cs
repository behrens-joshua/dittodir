﻿namespace DittoDir.DataAccess.Cryptography.MD5
{
    using System;
    using System.Diagnostics.Contracts;
    using System.Security.Cryptography;

    /// <summary>
    /// Holds an MD5 checksum.
    /// </summary>
    /// <seealso cref="IChecksum" />
    public class Checksum : IChecksum
    {
        /// <summary>
        /// Holds the checksum.
        /// </summary>
        private string _checksum = string.Empty;

        /// <summary>
        /// Gets the checksum.
        /// </summary>
        string IChecksum.Checksum
        {
            [Pure]
            get
            {
                Contract.Ensures(Contract.Result<string>() != null);

                return _checksum;
            }
        }

        /// <summary>
        /// Aggregates the specified stream into a <see cref="IChecksum"/>.
        /// </summary>
        /// <param name="stream">The stream to aggregate to.</param>
        /// <returns>A <see cref="IChecksum"/>-instance containing the checksum.</returns>
        public static IChecksum Aggregate(System.IO.Stream stream)
        {
            if (stream == null)
            {
                throw new ArgumentNullException(nameof(stream));
            }
            
            Contract.Ensures(Contract.Result<IChecksum>() != null);
            Contract.Ensures(Contract.Result<IChecksum>() is Checksum);

            if (!stream.CanRead)
            {
                throw new ArgumentException("Stream is not readable", nameof(stream));
            }

            IChecksum result = new Checksum();

            using (HashAlgorithm algo = MD5.Create())
            {
                // BitConverter.ToString(byte[]).Replace("-", string.Empty) is not a fast solution but there is no need to optimize it yet.
                // Check out https://github.com/patridge/PerformanceStubs/blob/61e4216515f3db041b7d7d79c14417dd24472ef7/PerformanceStubs/Tests/ConvertByteArrayToHexString/Test.cs#L137
                (result as Checksum)._checksum = BitConverter.ToString(algo.ComputeHash(stream)).Replace("-", string.Empty);
            }

            return result;
        }
    }
}
