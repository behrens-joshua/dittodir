﻿using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("DittoDir DataAccess")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("")]
[assembly: AssemblyProduct("DittoDir")]
[assembly: AssemblyCopyright("Copyright © Thorsten Gerds, Joshua Behrens 2016")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

[assembly: ComVisible(false)]
[assembly: Guid("79bba179-7a63-41ad-ab67-ba30b4f8bbb9")]
[assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyFileVersion("1.0")]
