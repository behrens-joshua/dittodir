﻿namespace DittoDir.DataAccess.FileSystem
{
    /// <summary>
    /// Holds meta information on a file.
    /// </summary>
    /// <seealso cref="IAmFileSystemObject" />
    public interface IAmFile : IAmFileSystemObject
    {
        /// <summary>
        /// Gets the filesize.
        /// </summary>
        long Filesize { get; }

        /// <summary>
        /// Calculates the Checksum for this file.
        /// </summary>
        /// <returns></returns>
        Cryptography.IChecksum CalculateChecksum();

        /// <summary>
        /// Gets the file extension.
        /// </summary>
        /// <value>
        /// The file extension.
        /// </value>
        string FileExtension { get; }

        /// <summary>
        /// Reads the bytes.
        /// </summary>
        /// <param name="amount">The amount.</param>
        /// <returns>
        /// A byte array with the given amount of bytes.<br />
        /// If the file contains less than amount bytes then only the available bytes will be returned.
        /// </returns>
        byte[] ReadBytes(int amount);

        /// <summary>
        /// Writes the bytes to the file.
        /// </summary>
        /// <param name="content">The content.</param>
        void WriteBytes(byte[] content);
    }
}
