﻿namespace DittoDir.DataAccess.FileSystem.Local
{
    using System;
    using System.Linq;
    using System.Collections.Generic;
    using System.Diagnostics.Contracts;

    /// <summary>
    /// Is able to resolve <see cref="Uri"/> pointing to file on the local filesystem to a <see cref="IAmFileSystemObject"/>.
    /// </summary>
    /// <seealso cref="ITryResolveUriToFileSystemObject" />
    internal class FileSystemObjectFactory : ITryResolveUriToFileSystemObject
    {
        /// <summary>
        /// Tries the resolve URI to a readable <see cref="IAmFileSystemObject" />.
        /// </summary>
        /// <param name="geller">The uri to analyze and resolve.</param>
        /// <param name="fso">The resulting <see cref="IAmFileSystemObject" />. Is null if unable to resolve.</param>
        /// <returns>
        ///   <c>True</c>, if the uri had been resolved to a valid <see cref="IAmFileSystemObject" /> instance, otherwise <c>false</c>.
        /// </returns>
        public bool TryResolveUriForReading(Uri geller, out IAmFileSystemObject fso)
        {
            Contract.EnsuresOnThrow<Exception>(Contract.ValueAtReturn(out fso) == null);
            Contract.Assume(geller != null);

            fso = null;

            if (geller.Scheme == Uri.UriSchemeFile)
            {
                if (System.IO.Directory.Exists(geller.LocalPath))
                {
                    fso = new Directory(geller.LocalPath);
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Tries the resolve URI to a writable <see cref="IAmFileSystemObject" />.
        /// </summary>
        /// <param name="geller">The uri to analyze and resolve.</param>
        /// <param name="root">The root <see cref="IAmDirectory"/> to refer to.</param>
        /// <param name="fso">The resulting <see cref="IAmFileSystemObject" />. Is null if unable to resolve.</param>
        /// <returns>
        ///   <c>True</c>, if the uri had been resolved to a valid <see cref="IAmFileSystemObject" /> instance, otherwise <c>false</c>.
        /// </returns>
        public bool TryResolveUriForWriting(Uri geller, IAmDirectory root, out IAmFileSystemObject fso)
        {
            Contract.EnsuresOnThrow<Exception>(Contract.ValueAtReturn(out fso) == null);
            Contract.Assume(geller != null);

            fso = null;

            bool isDirectory = geller.OriginalString.EndsWith("/", StringComparison.CurrentCulture) || geller.OriginalString.EndsWith("\\", StringComparison.CurrentCulture);

            if (root == null && geller.IsAbsoluteUri && geller.Scheme == Uri.UriSchemeFile && isDirectory)
            {
                // we have create a root element

                if (!System.IO.Directory.Exists(geller.LocalPath))
                {
                    fso = new Directory(System.IO.Directory.CreateDirectory(geller.LocalPath));
                }
                else
                {
                    fso = new Directory(new System.IO.DirectoryInfo(geller.LocalPath));
                }
                
                return true;
            }

            if ((root as Directory) != null && !geller.IsAbsoluteUri)
            {
                // we have to create a rooted element

                string fullPath = System.IO.Path.Combine(root.Path, geller.OriginalString);

                List<string> parents = new List<string>();

                string parentPath = fullPath;

                do
                {
                    parentPath = System.IO.Path.GetDirectoryName(fullPath);
                    parents.Add(parentPath);
                }
                while (parentPath != root.Path);

                parents.Reverse();

                IAmDirectory parent = parents.Aggregate(root, (parentFso, path) => new Directory(path, parentFso));

                if (isDirectory)
                {
                    fso = new Directory(fullPath, parent);
                }
                else
                {
                    fso = new File(fullPath, parent);
                }

                return true;
            }

            return false;
        }
    }
}
