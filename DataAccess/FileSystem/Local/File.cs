﻿namespace DittoDir.DataAccess.FileSystem.Local
{
    using System;
    using Cryptography;
    using System.Diagnostics.Contracts;

    /// <summary>
    /// Holds meta information on a file from a local filesystem.
    /// </summary>
    /// <seealso cref="IAmFile" />
    internal class File : IAmFile
    {
        /// <summary>
        /// Holds the calculated <see cref="IChecksum" />.
        /// </summary>
        private IChecksum _checksum;

        /// <summary>
        /// Gets the creation date.
        /// </summary>
        public DateTime CreationDate { [Pure] get; }

        /// <summary>
        /// Gets the filesize.
        /// </summary>
        public long Filesize { [Pure] get; }

        /// <summary>
        /// Gets the last changed date.
        /// </summary>
        public DateTime LastChangedDate { [Pure] get; }

        /// <summary>
        /// Gets the parent path.
        /// </summary>
        public string ParentPath
        { 
            [Pure]
            get
            {
                Contract.Ensures(Contract.Result<string>() != null);

                return Parent?.Path ?? string.Empty;
            }
        }

        /// <summary>
        /// Gets the path.
        /// </summary>
        public string Path { [Pure] get; private set; }

        /// <summary>
        /// Gets whether or not this is the root element
        /// </summary>
        public bool IsRoot
        {
            [Pure]
            get
            {
                return false;
            }
        }

        /// <summary>
        /// Gets the file extension.
        /// </summary>
        public string FileExtension
        {
            [Pure]
            get
            {
                Contract.Ensures(Contract.Result<string>() != null);

                return System.IO.Path.GetExtension(Path);
            }
        }

        /// <summary>
        /// Gets the relative path to the root object.<br />
        /// If this is the root object, then it return an empty string.
        /// </summary>
        public string RelativePath
        {
            [Pure]
            get
            {
                Contract.Ensures(Contract.Result<string>() != null);

                return Parent?.RelativePath + Name;
            }
        }

        /// <summary>
        /// Gets the parent.
        /// </summary>
        public IAmFileSystemObject Parent { [Pure] get; }

        /// <summary>
        /// Gets the name.
        /// </summary>
        public string Name { [Pure] get; }

        /// <summary>
        /// Initializes a new instance of the <see cref="File" /> class.
        /// </summary>
        /// <param name="filename">The filename of the file to analyze.</param>
        /// <param name="parent">The parent filesystem object.</param>
        public File(string filename, IAmFileSystemObject parent) : this(new System.IO.FileInfo(filename), parent) { }

        /// <summary>
        /// Initializes a new instance of the <see cref="File" /> class.
        /// </summary>
        /// <param name="fileinfo">The <see cref="System.IO.FileInfo" /> of the file to analyze.</param>
        /// <param name="parent">The parent filesystem object.</param>
        public File(System.IO.FileInfo fileinfo, IAmFileSystemObject parent)
        {
            Contract.Requires(fileinfo != null);
            Contract.Requires(parent != null);
            Contract.Ensures(this.Parent != null);
            Contract.Ensures(this.Name != null);
            Contract.Ensures(this.Filesize >= 0);
            Contract.Ensures(this.Path != null);

            if (!fileinfo.Exists)
            {
                throw new ArgumentException("File must exist", nameof(fileinfo));
            }

            Parent = parent;
            Name = fileinfo.Name ?? string.Empty;
            CreationDate = fileinfo.CreationTime;
            Filesize = Math.Max(fileinfo.Length, 0);
            LastChangedDate = fileinfo.LastWriteTime;
            Path = fileinfo.FullName ?? string.Empty;
        }

        /// <summary>
        /// Calculates the checksum of this file.
        /// </summary>
        /// <returns>An <see cref="IChecksum"/> instance.</returns>
        public IChecksum CalculateChecksum()
        {
            Contract.Ensures(Contract.Result<IChecksum>() != null);
            Contract.Ensures(Contract.Result<IChecksum>() is Cryptography.MD5.Checksum);

            if (_checksum == null)
            {
                using (System.IO.Stream filestream = System.IO.File.OpenRead(Path))
                {
                    _checksum = Cryptography.MD5.Checksum.Aggregate(filestream);
                }
            }

            return _checksum;
        }

        /// <summary>
        /// Reads the bytes.
        /// </summary>
        /// <param name="amount">The amount.</param>
        /// <returns>
        /// A byte array with the given amount of bytes.<br />
        /// If the file contains less than amount bytes then only the available bytes will be returned.
        /// </returns>
        public byte[] ReadBytes(int amount)
        {
            Contract.Requires(amount > 0);
            Contract.Ensures(Contract.Result<byte[]>() != null);
            Contract.Ensures(Contract.Result<byte[]>().Length == amount);

            byte[] buffer = new byte[amount];
            using (System.IO.Stream filestream = System.IO.File.OpenRead(Path))
            {
                filestream.Read(buffer, 0, amount);
            }

            return buffer;
        }

        /// <summary>
        /// Writes the bytes to the file.
        /// </summary>
        /// <param name="content">The content.</param>
        public void WriteBytes(byte[] content)
        {
            Contract.Requires<ArgumentNullException>(content != null);
            Contract.Requires<ArgumentException>(content.Length != 0);

            using(System.IO.Stream filestream = System.IO.File.OpenWrite(Path))
            {
                filestream.Write(content, 0, content.Length);
                filestream.Flush();
            }
        }

        /// <summary>
        /// Changes the root path.
        /// </summary>
        /// <param name="newRootPath">The new root path.</param>
        public void ChangeRootPath(string newRootPath)
        {
            Contract.Requires<ArgumentNullException>(newRootPath != null);
            Contract.Requires<ArgumentException>(newRootPath != string.Empty);

            this.Path = System.IO.Path.Combine(newRootPath, RelativePath);
        }

        /// <summary>
        /// Creates the file or folder referenced by this instance.
        /// </summary>
        public void Create()
        {
            Contract.Requires<NullReferenceException>(this.Path != null);

            System.IO.File.Create(this.Path);
        }

        /// <summary>
        /// Sets the creation date.
        /// </summary>
        /// <param name="dateTime">The creation time.</param>
        public void SetCreationDate(DateTime dateTime)
        {
            Contract.Requires<ArgumentException>(dateTime == DateTime.MinValue);
            Contract.Requires<NullReferenceException>(this.Path != null);

            System.IO.File.SetCreationTime(this.Path, dateTime);
        }

        /// <summary>
        /// Sets the last changed date.
        /// </summary>
        /// <param name="dateTime">The change time.</param>
        public void SetLastChangedDate(DateTime dateTime)
        {
            Contract.Requires<ArgumentException>(dateTime == DateTime.MinValue);
            Contract.Requires<NullReferenceException>(this.Path != null);

            System.IO.File.SetLastWriteTime(this.Path, dateTime);
        }
    }
}
