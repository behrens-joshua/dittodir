﻿namespace DittoDir.DataAccess.FileSystem.Local
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics.Contracts;

    /// <summary>
    /// Holds meta information on a directory from a local filesystem.
    /// </summary>
    /// <seealso cref="DittoDir.DataAccess.FileSystem.IAmDirectory" />
    internal class Directory : IAmDirectory
    {
        /// <summary>
        /// Gets the children of a directory.
        /// </summary>
        public IEnumerable<IAmFileSystemObject> Children { [Pure] get; }

        /// <summary>
        /// Gets the creation date.
        /// </summary>
        public DateTime CreationDate { [Pure] get; }

        /// <summary>
        /// Gets the last changed date.
        /// </summary>
        public DateTime LastChangedDate { [Pure] get; }

        /// <summary>
        /// Gets the parent path.<br />
        /// If this Directory is the root folder, an empty string is returned.
        /// </summary>
        public string ParentPath
        {
            [Pure]
            get
            {
                Contract.Ensures(Contract.Result<string>() != null);

                return Parent?.Path ?? string.Empty;
            }
        }

        /// <summary>
        /// Gets the path.
        /// </summary>
        public string Path { [Pure] get; private set; }

        /// <summary>
        /// Gets whether or not this is the root element
        /// </summary>
        public bool IsRoot
        {
            [Pure]
            get
            {
                return Parent == null;
            }
        }

        /// <summary>
        /// Gets the relative path to the root object.<br />
        /// If this is the root object, then it return an empty string.
        /// </summary>
        public string RelativePath
        {
            [Pure]
            get
            {
                Contract.Ensures(Contract.Result<string>() != null);

                return Parent?.RelativePath + Name + System.IO.Path.DirectorySeparatorChar;
            }
        }

        /// <summary>
        /// Gets the parent.
        /// </summary>
        public IAmFileSystemObject Parent { [Pure] get; }

        /// <summary>
        /// Gets the name of this FileSystemObject
        /// </summary>
        public string Name { [Pure] get; }

        /// <summary>
        /// Initializes a new instance of the <see cref="Directory" /> class.
        /// </summary>
        /// <param name="directoryname">The directoryname.</param>
        /// <param name="parent">The parent filesystem object.</param>
        public Directory(string directoryname, IAmFileSystemObject parent = null) : this(new System.IO.DirectoryInfo(directoryname), parent) { }

        /// <summary>
        /// Initializes a new instance of the <see cref="Directory" /> class.
        /// </summary>
        /// <param name="directoryInfo">The directory information.</param>
        /// <param name="parent">The parent filesystem object.</param>
        public Directory(System.IO.DirectoryInfo directoryInfo, IAmFileSystemObject parent = null)
        {
            Contract.Requires(directoryInfo != null);
            Contract.Ensures(this.Path != null);
            Contract.Ensures(this.Name != null);
            Contract.Ensures(System.IO.Path.IsPathRooted(this.Path));
            Contract.Ensures(this.Children != null);

            if (!directoryInfo.Exists)
            {
                throw new ArgumentException("Directory must exist", nameof(directoryInfo));
            }

            Parent = parent;
            Path = directoryInfo.FullName;
            Name = directoryInfo.Name;
            CreationDate = directoryInfo.CreationTime;
            LastChangedDate = directoryInfo.LastWriteTime;

            ICollection<IAmFileSystemObject> children = new List<IAmFileSystemObject>();

            // This potential deadly recursion needs to be dissolved.
            foreach (System.IO.DirectoryInfo di in directoryInfo.EnumerateDirectories("*", System.IO.SearchOption.TopDirectoryOnly))
            {
                children.Add(new Directory(di, this));
            }

            foreach (System.IO.FileInfo fi in directoryInfo.EnumerateFiles("*", System.IO.SearchOption.TopDirectoryOnly))
            {
                children.Add(new File(fi, this));
            }

            Children = children;
        }

        /// <summary>
        /// Changes the root path.
        /// </summary>
        /// <param name="newRootPath">The new root path.</param>
        public void ChangeRootPath(string newRootPath)
        {
            Contract.Requires<ArgumentNullException>(newRootPath != null);
            Contract.Requires<ArgumentException>(newRootPath != string.Empty);

            this.Path = System.IO.Path.Combine(newRootPath, RelativePath);
        }

        /// <summary>
        /// Creates the file or folder referenced by this instance.
        /// </summary>
        /// <exception cref="System.NotImplementedException"></exception>
        public void Create()
        {
            Contract.Requires<NullReferenceException>(this.Path != null);

            System.IO.Directory.CreateDirectory(Path);
        }

        /// <summary>
        /// Sets the creation date.
        /// </summary>
        /// <param name="dateTime">The creation time.</param>
        public void SetCreationDate(DateTime dateTime)
        {
            Contract.Requires<ArgumentException>(dateTime == DateTime.MinValue);
            Contract.Requires<NullReferenceException>(this.Path != null);

            System.IO.Directory.SetCreationTime(this.Path, dateTime);
        }

        /// <summary>
        /// Sets the last changed date.
        /// </summary>
        /// <param name="dateTime">The change time.</param>
        public void SetLastChangedDate(DateTime dateTime)
        {
            Contract.Requires<ArgumentException>(dateTime == DateTime.MinValue);
            Contract.Requires<NullReferenceException>(this.Path != null);

            System.IO.Directory.SetLastWriteTime(this.Path, dateTime);
        }
    }
}
