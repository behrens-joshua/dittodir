﻿namespace DittoDir.DataAccess.FileSystem
{
    /// <summary>
    /// Holds meta information on a directory.
    /// </summary>
    /// <seealso cref="IAmFileSystemObject" />
    public interface IAmDirectory : IAmFileSystemObject
    {
        /// <summary>
        /// Gets the children of a directory.
        /// </summary>
        System.Collections.Generic.IEnumerable<IAmFileSystemObject> Children { get; }
    }
}
