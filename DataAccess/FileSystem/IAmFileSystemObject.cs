﻿namespace DittoDir.DataAccess.FileSystem
{
    using System;

    /// <summary>
    /// Holds meta information on an object from any filesystem.
    /// </summary>
    public interface IAmFileSystemObject
    {
        /// <summary>
        /// Gets the path.
        /// </summary>
        string Path { get; }

        /// <summary>
        /// Gets the parent path.
        /// </summary>
        string ParentPath { get; }

        /// <summary>
        /// Gets the parent.
        /// </summary>
        IAmFileSystemObject Parent { get; }

        /// <summary>
        /// Gets the name of this FileSystemObject
        /// </summary>
        string Name { get; }

        /// <summary>
        /// Gets the relative path to the root object.<br />
        /// If this is the root object, then it return an empty string.
        /// </summary>
        string RelativePath { get; }

        /// <summary>
        /// Gets whether or not this is the root element
        /// </summary>
        bool IsRoot { get; }

        /// <summary>
        /// Gets the creation date.
        /// </summary>
        DateTime CreationDate { get; }

        /// <summary>
        /// Gets the last changed date.
        /// </summary>
        DateTime LastChangedDate { get; }

        /// <summary>
        /// Changes the root path.
        /// </summary>
        /// <param name="newRootPath">The new root path.</param>
        void ChangeRootPath(string newRootPath);

        /// <summary>
        /// Creates the file or folder referenced by this instance.
        /// </summary>
        void Create();

        /// <summary>
        /// Sets the creation date.
        /// </summary>
        /// <param name="dateTime">The creation time.</param>
        void SetCreationDate(DateTime dateTime);

        /// <summary>
        /// Sets the last changed date.
        /// </summary>
        /// <param name="dateTime">The change time.</param>
        void SetLastChangedDate(DateTime dateTime);
    }
}
