﻿namespace DittoDir.DataAccess.FileSystem
{
    using System;

    /// <summary>
    /// Is able to try to resolve any given <see cref="Uri"/> into a <see cref="IAmFileSystemObject"/>.
    /// </summary>
    internal interface ITryResolveUriToFileSystemObject
    {
        /// <summary>
        /// Tries the resolve URI to a readable <see cref="IAmFileSystemObject"/>.
        /// </summary>
        /// <param name="geller">The uri to analyze and resolve.</param>
        /// <param name="fso">The resulting <see cref="IAmFileSystemObject"/>. Is null if unable to resolve.</param>
        /// <returns><c>True</c>, if the uri had been resolved to a valid <see cref="IAmFileSystemObject"/> instance, otherwise <c>false</c>.</returns>
        bool TryResolveUriForReading(Uri geller, out IAmFileSystemObject fso);

        /// <summary>
        /// Tries the resolve URI to a writable <see cref="IAmFileSystemObject"/>.
        /// </summary>
        /// <param name="geller">The uri to analyze and resolve.</param>
        /// <param name="root">The root <see cref="IAmDirectory"/> to refer to.</param>
        /// <param name="fso">The resulting <see cref="IAmFileSystemObject"/>. Is null if unable to resolve.</param>
        /// <returns><c>True</c>, if the uri had been resolved to a valid <see cref="IAmFileSystemObject"/> instance, otherwise <c>false</c>.</returns>
        bool TryResolveUriForWriting(Uri geller, IAmDirectory root, out IAmFileSystemObject fso);
    }
}
