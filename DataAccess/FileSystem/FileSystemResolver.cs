﻿namespace DittoDir.DataAccess.FileSystem
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics.Contracts;
    using System.Linq;
    using System.Reflection;

    public static class FileSystemResolver
    {
        /// <summary>
        /// All available <see cref="ITryResolveUriToFileSystemObject"/>.
        /// </summary>
        private static readonly IEnumerable<ITryResolveUriToFileSystemObject> _uriResolver;

        /// <summary>
        /// Initializes the <see cref="FileSystemResolver"/> class.
        /// </summary>
        static FileSystemResolver()
        {
            // if indentified as slow call iter of assembly types asynchronously
            _uriResolver = Assembly.GetAssembly(typeof(FileSystemResolver)).
                                    GetTypes().
                                    Where(p => !p.IsAbstract && p.GetInterfaces().Contains(typeof(ITryResolveUriToFileSystemObject))).
                                    Where(p => p.GetConstructor(Type.EmptyTypes) != null).
                                    Select(p => Activator.CreateInstance(p) as ITryResolveUriToFileSystemObject).
                                    Where(p => p != null).
                                    ToArray();
        }

        /// <summary>
        /// Resolves the URI for reading.
        /// </summary>
        /// <param name="uri">The URI to resolve.</param>
        /// <returns>An <see cref="IAmFileSystemObject"/> instance if <paramref name="uri"/> was resolvable by any resolver, otherwise false.</returns>
        public static IAmFileSystemObject ReadUri(Uri uri)
        {
            Contract.Assume(uri != null);

            IAmFileSystemObject result = null;

            return _uriResolver.Any(p => p.TryResolveUriForReading(uri, out result)) ? result : null;
        }

        /// <summary>
        /// Resolves the URI for writing.
        /// </summary>
        /// <param name="uri">The URI to resolve.</param>
        /// <param name="root">The root element of the Uri to work in. If null <paramref name="uri"/> is expected to reference a root directory.</param>
        /// <returns>An <see cref="IAmFileSystemObject"/> instance if <paramref name="uri"/> was resolvable by any resolver, otherwise false.</returns>
        public static IAmFileSystemObject WriteUri(Uri uri, IAmDirectory root)
        {
            Contract.Assume(uri != null);

            IAmFileSystemObject result = null;

            return _uriResolver.Any(p => p.TryResolveUriForWriting(uri, root, out result)) ? result : null;
        }
    }
}
