﻿using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("DittoDir Abstraction")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("")]
[assembly: AssemblyProduct("DittoDir")]
[assembly: AssemblyCopyright("Copyright © Thorsten Gerds, Joshua Behrens 2016")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

[assembly: ComVisible(false)]
[assembly: Guid("a335af6b-2631-4580-87c2-a78e915f912b")]
[assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyFileVersion("1.0")]