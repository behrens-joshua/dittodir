﻿namespace DittoDir.BusinessLogic.Processing
{
    using System.Collections.Generic;

    /// <summary>
    /// Describes a FileType
    /// </summary>
    public interface IFileType
    {
        /// <summary>
        /// Gets the Name of the FileType
        /// </summary>
        string Name { get; }

        bool IsFolder { get; }

        /// <summary>
        /// Gets an enumeration of all FileExtensions
        /// </summary>
        IEnumerable<string> FileExtension { get; }

        /// <summary>
        /// Gets an enumeration of all MagicNumbers
        /// </summary>
        IEnumerable<byte[]> MagicNumber { get; }
    }
}
