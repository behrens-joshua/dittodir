﻿namespace DittoDir.BusinessLogic.Processing
{
    /// <summary>
    /// Instanciates ReadStrategies
    /// </summary>
    public interface IReadStrategyFactory
    {
        /// <summary>
        /// Creates the read strategy.
        /// </summary>
        /// <param name="fileType">Type of the file.</param>
        /// <returns>An instance of <see cref="IReadStrategy"/></returns>
        IReadStrategy CreateReadStrategy(IFileType fileType);
    }
}
