﻿namespace DittoDir.BusinessLogic.Processing
{
    /// <summary>
    /// Represents a write strategy
    /// </summary>
    public interface IWriteStrategy
    {
        /// <summary>
        /// Writes metadata.
        /// </summary>
        /// <param name="metaData">The meta data.</param>
        /// <param name="options">The options.</param>
        void Write(IMetaData metaData, IStrategyOptions options);

        /// <summary>
        /// Gets the specific options.
        /// </summary>
        /// <returns>A new instance of the strategy options</returns>
        IStrategyOptions GetOptions();
    }
}
