﻿namespace DittoDir.BusinessLogic.Processing
{

    /// <summary>
    /// Instanciates WriteStrategies
    /// </summary>
    public interface IWriteStrategyFactory
    {
        /// <summary>
        /// Creates the write strategy.
        /// </summary>
        /// <param name="fileType">Type of the file.</param>
        /// <returns>An instance of <see cref="IWriteStrategy"/></returns>
        IWriteStrategy CreateWriteStrategy(IFileType fileType);
    }
}
