﻿namespace DittoDir.BusinessLogic.Processing
{
    using System;

    /// <summary>
    /// Resolves a read strategy by a <see cref="IFileType" /> instance.
    /// </summary>
    public interface IReadStrategyResolver
    {
        /// <summary>
        /// Gets a <see cref="IReadStrategy"/>.
        /// </summary>
        /// <param name="fileType">A <see cref="IFileType"/> instance.</param>
        /// <returns>A <see cref="IReadStrategy"/> instance that handles the fileType.</returns>
        Type GetReadStrategy(IFileType fileType);

        /// <summary>
        /// Registers a new <see cref="IReadStrategy"/> for a <see cref="IFileType"/>.
        /// </summary>
        /// <param name="fileType">The FileType</param>
        /// <param name="strategy">The ReadStrategy</param>
        void RegisterReadStrategy(IFileType fileType, Type strategy);

        /// <summary>
        /// Deregisters the read strategy.
        /// </summary>
        /// <param name="fileType">Type of the file.</param>
        void DeregisterReadStrategy(IFileType fileType);
    }
}
