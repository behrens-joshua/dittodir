﻿namespace DittoDir.BusinessLogic.Processing
{ 
    using DataAccess.FileSystem;

    /// <summary>
    /// Resolves a FileType from a File
    /// </summary>
    public interface IFileTypeResolver
    {
        /// <summary>
        /// Resolves a <see cref="IAmFile"/> to a <see cref="IFileType"/>
        /// </summary>
        /// <param name="file">Any <see cref="IAmFile"/> instance.</param>
        /// <returns>An instance of <see cref="IFileType"/>.</returns>
        IFileType Resolve(IAmFileSystemObject file);

        /// <summary>
        /// Registers a new <see cref="IFileType" />.<br />
        /// Every filetype must be registered first.
        /// </summary>
        /// <param name="fileTypeDescriptor">A <see cref="IFileType" /> instance.</param>
        void Register(IFileType fileTypeDescriptor);

        /// <summary>
        /// Registers a new <see cref="IFileType" />.<br />
        /// Every filetype must be registered first.
        /// </summary>
        /// <param name="fileTypeDescriptor">A <see cref="IFileType" /> instance.</param>
        void Deregister(IFileType fileTypeDescriptor);
    }
}
