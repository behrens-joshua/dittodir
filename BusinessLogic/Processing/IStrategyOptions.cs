﻿using System.ComponentModel;

namespace DittoDir.BusinessLogic.Processing
{
    /// <summary>
    /// Stores the strategy options<br />
    /// The properties in this interface are minimum options for all strategies.<br />
    /// An implementation of this strategy may provide more options.
    /// </summary>
    public interface IStrategyOptions : INotifyPropertyChanged
    {
        /// <summary>
        /// Gets or sets a value indicating whether [copy creation date].
        /// </summary>
        bool CopyCreationDate { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [copy change date].
        /// </summary>
        bool CopyChangeDate { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [mock size].
        /// </summary>
        bool MockSize { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [copy meta data].
        /// </summary>
        bool CopyMetaData { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether to copy magic number or not.
        /// </summary>
        bool CopyMagicNumber { get; set; }
    }
}
