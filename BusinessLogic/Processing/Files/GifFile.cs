﻿namespace DittoDir.BusinessLogic.Processing.Files
{
    using System.Collections.Generic;
    using System.Diagnostics.Contracts;
    using System.Linq;

    /// <summary>
    /// Descriptor for Gif Files
    /// </summary>
    /// <seealso cref="IFileType" />
    public class GifFile : IFileType
    {
        /// <summary>
        /// Gets an enumeration of all FileExtensions
        /// </summary>
        public IEnumerable<string> FileExtension
        {
            [Pure]
            get
            {
                Contract.Ensures(Contract.Result<IEnumerable<string>>() != null);
                Contract.Ensures(Contract.Result<IEnumerable<string>>().Count() == 1);

                yield return "*.gif";
            }
        }

        /// <summary>
        /// Gets a value indicating whether this instance is folder.
        /// </summary>
        public bool IsFolder
        {
            [Pure]
            get
            {
                return false;
            }
        }

        /// <summary>
        /// Gets an enumeration of all MagicNumbers
        /// </summary>
        public IEnumerable<byte[]> MagicNumber
        {
            [Pure]
            get
            {
                Contract.Ensures(Contract.Result<IEnumerable<byte[]>>() != null);
                Contract.Ensures(Contract.Result<IEnumerable<byte[]>>().Count() == 2);
                Contract.Ensures(Contract.ForAll(Contract.Result<IEnumerable<byte[]>>(), p => p != null));

                yield return new byte[] { 0x47, 0x49, 0x46, 0x38, 0x39, 0x61 }; // GIF89a
                yield return new byte[] { 0x47, 0x49, 0x46, 0x38, 0x37, 0x61 }; // GIF87a
            }
        }

        /// <summary>
        /// Gets the Name of the FileType
        /// </summary>
        public string Name
        {
            [Pure]
            get
            {
                Contract.Ensures(Contract.Result<string>() != null);

                return "Compuserve GIF file";
            }
        }
    }
}
