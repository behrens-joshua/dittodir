﻿namespace DittoDir.BusinessLogic.Processing.Files
{
    using System.Collections.Generic;
    using System.Diagnostics.Contracts;
    using System.Linq;

    /// <summary>
    /// Descriptor for PDF Files
    /// </summary>
    /// <seealso cref="IFileType" />
    public class PdfFile : IFileType
    {
        /// <summary>
        /// Gets an enumeration of all FileExtensions
        /// </summary>
        public IEnumerable<string> FileExtension
        {
            [Pure]
            get
            {
                Contract.Ensures(Contract.Result<IEnumerable<string>>() != null);
                Contract.Ensures(Contract.Result<IEnumerable<string>>().Count() == 1);

                yield return "*.pdf";
            }
        }

        /// <summary>
        /// Gets a value indicating whether this instance is folder.
        /// </summary>
        public bool IsFolder
        {
            [Pure]
            get
            {
                return false;
            }
        }

        /// <summary>
        /// Gets an enumeration of all MagicNumbers
        /// </summary>
        public IEnumerable<byte[]> MagicNumber
        {
            [Pure]
            get
            {
                Contract.Ensures(Contract.Result<IEnumerable<byte[]>>() != null);
                Contract.Ensures(Contract.Result<IEnumerable<byte[]>>().Count() == 1);
                Contract.Ensures(Contract.ForAll(Contract.Result<IEnumerable<byte[]>>(), p => p != null));

                yield return new byte[] { 0x25, 0x50, 0x44, 0x46 }; // %PDF
            }
        }

        /// <summary>
        /// Gets the Name of the FileType
        /// </summary>
        public string Name
        {
            [Pure]
            get
            {
                Contract.Ensures(Contract.Result<string>() != null);

                return "Adobe PDF File";
            }
        }
    }
}
