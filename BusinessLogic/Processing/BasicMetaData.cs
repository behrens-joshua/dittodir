﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DittoDir.DataAccess.FileSystem;

namespace DittoDir.BusinessLogic.Processing
{
    /// <summary>
    /// Basic implementation of IMetaData
    /// </summary>
    /// <seealso cref="DittoDir.BusinessLogic.Processing.IMetaData" />
    public class BasicMetaData : IMetaData
    {
        /// <summary>
        /// Gets the <see cref="IFileType" />.
        /// </summary>
        public IFileType FileType
        {
            get; set;
        }

        /// <summary>
        /// Gets the <see cref="IAmFileSystemObject" />.
        /// </summary>
        public IAmFileSystemObject Item
        {
            get; set;
        }

        /// <summary>
        /// Gets the MetaData of the Item.
        /// </summary>
        public IDictionary<string, object> MetaData
        {
            get; set;
        }
    }
}
