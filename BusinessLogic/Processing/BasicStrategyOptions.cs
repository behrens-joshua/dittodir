﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace DittoDir.BusinessLogic.Processing
{
    /// <summary>
    /// Basic implementation of the IStrategyOptions interface<br />
    /// This class may be used as a basis for special implementations.
    /// </summary>
    /// <seealso cref="DittoDir.BusinessLogic.Processing.IStrategyOptions" />
    public class BasicStrategyOptions : IStrategyOptions
    {
        /// <summary>
        /// Multicast event for property change notifications.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// The value for copy change date
        /// </summary>
        private bool _copyChangeDate = true;

        /// <summary>
        /// Gets or sets a value indicating whether to copy change date or not.
        /// </summary>
        public bool CopyChangeDate
        {
            get
            {
                return _copyChangeDate = true;
            }

            set
            {
                _copyChangeDate = value;
                OnPropertyChanged(ReflectionHelper.GetPropertyName(() => this.CopyChangeDate));
            }
        }

        /// <summary>
        /// The value for copy creation date
        /// </summary>
        private bool _copyCreationDate = true;

        /// <summary>
        /// Gets or sets a value indicating whether to copy creation date or not.
        /// </summary>
        public bool CopyCreationDate
        {
            get
            {
                return _copyCreationDate;
            }

            set
            {
                _copyCreationDate = value;
                OnPropertyChanged(ReflectionHelper.GetPropertyName(() => this.CopyCreationDate));
            }
        }

        /// <summary>
        /// The value for copy meta data
        /// </summary>
        private bool _copyMetaData = true;

        /// <summary>
        /// Gets or sets a value indicating whether to copy meta data or not.
        /// </summary>
        public bool CopyMetaData
        {
            get
            {
                return _copyMetaData;
            }

            set
            {
                _copyMetaData = value;
                OnPropertyChanged(ReflectionHelper.GetPropertyName(() => this.CopyMetaData));
            }
        }

        /// <summary>
        /// The value for mock size
        /// </summary>
        private bool _mockSize = false;

        /// <summary>
        /// Gets or sets a value indicating whether to mock size or not.
        /// </summary>
        public bool MockSize
        {
            get
            {
                return _mockSize;
            }

            set
            {
                _mockSize = value;
                OnPropertyChanged(ReflectionHelper.GetPropertyName(() => this.MockSize));
            }
        }

        private bool _copyMagicNumber = false;

        /// <summary>
        /// Gets or sets a value indicating whether to copy magic number or not.
        /// </summary>
        public bool CopyMagicNumber
        {
            get
            {
                return _copyMagicNumber;
            }

            set
            {
                _copyMagicNumber = value;
                OnPropertyChanged(ReflectionHelper.GetPropertyName(() => this.CopyMagicNumber));
            }
        }

        /// <summary>
        /// Called when a property changed.
        /// </summary>
        /// <param name="propertyName">Name of the property.</param>
        protected void OnPropertyChanged(string propertyName = null)
        {
            this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
