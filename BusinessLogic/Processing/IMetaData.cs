﻿namespace DittoDir.BusinessLogic.Processing
{
    using DataAccess.FileSystem;
    using System.Collections.Generic;

    /// <summary>
    /// Holds the MetaData of a <see cref="IAmFileSystemObject"/>
    /// </summary>
    public interface IMetaData
    {
        /// <summary>
        /// Gets the <see cref="IAmFileSystemObject"/>.
        /// </summary>
        IAmFileSystemObject Item { get; }

        /// <summary>
        /// Gets the <see cref="IFileType"/>.
        /// </summary>
        IFileType FileType { get; }

        /// <summary>
        /// Gets the MetaData of the Item.
        /// </summary>
        IDictionary<string, object> MetaData { get; }
    }
}
