﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Text;

namespace DittoDir.BusinessLogic.Processing
{
    /// <summary>
    /// A default implementation of IReadStrategyFactory and IWriteStrategyFactory
    /// </summary>
    /// <seealso cref="DittoDir.BusinessLogic.Processing.IReadStrategyFactory" />
    /// <seealso cref="DittoDir.BusinessLogic.Processing.IWriteStrategyFactory" />
    public class StrategyFactory : IReadStrategyFactory, IWriteStrategyFactory
    {
        /// <summary>
        /// Gets or sets the read strategy resolver.
        /// </summary>
        public IReadStrategyResolver ReadStrategyResolver { protected get; set; }

        /// <summary>
        /// Gets or sets the write strategy resolver.
        /// </summary>
        public IWriteStrategyResolver WriteStrategyResolver { protected get; set; }

        /// <summary>
        /// The read strategy instances
        /// </summary>
        private readonly Dictionary<IFileType, IReadStrategy> _readStrategyInstances = new Dictionary<IFileType, IReadStrategy>();

        /// <summary>
        /// The write strategy instances
        /// </summary>
        private readonly Dictionary<IFileType, IWriteStrategy> _writeStrategyInstances = new Dictionary<IFileType, IWriteStrategy>();

        /// <summary>
        /// Creates the read strategy.
        /// </summary>
        /// <param name="fileType">Type of the file.</param>
        /// <returns>
        /// An instance of <see cref="IReadStrategy" />
        /// </returns>
        public IReadStrategy CreateReadStrategy(IFileType fileType)
        {
            Contract.Requires<ArgumentNullException>(fileType != null);

            var strategy = _readStrategyInstances[fileType];
            if(strategy == null)
            {
                var strategyType = ReadStrategyResolver.GetReadStrategy(fileType);
                // Assume that the type is derived from IReadStrategy
                Contract.Assume(typeof(IReadStrategy).IsAssignableFrom(strategyType));
                // Assume there is a default constructor
                Contract.Assume(strategyType.GetConstructor(Type.EmptyTypes) != null);

                strategy = (IReadStrategy)Activator.CreateInstance(strategyType);
            }

            return strategy;
        }

        /// <summary>
        /// Creates the write strategy.
        /// </summary>
        /// <param name="fileType">Type of the file.</param>
        /// <returns>
        /// An instance of <see cref="IWriteStrategy" />
        /// </returns>
        public IWriteStrategy CreateWriteStrategy(IFileType fileType)
        {
            Contract.Requires<ArgumentNullException>(fileType != null);

            var strategy = _writeStrategyInstances[fileType];
            if (strategy == null)
            {
                var strategyType = ReadStrategyResolver.GetReadStrategy(fileType);
                // Assume that the type is derived from IWriteStrategy
                Contract.Assume(typeof(IWriteStrategy).IsAssignableFrom(strategyType));
                // Assume there is a default constructor
                Contract.Assume(strategyType.GetConstructor(Type.EmptyTypes) != null);

                strategy = (IWriteStrategy)Activator.CreateInstance(strategyType);
            }

            return strategy;
        }
    }
}
