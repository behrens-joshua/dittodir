﻿namespace DittoDir.BusinessLogic.Processing
{
    using System;

    /// <summary>
    /// Resolves a write strategy by a <see cref="IFileType" /> instance.
    /// </summary>
    public interface IWriteStrategyResolver
    {
        /// <summary>
        /// Gets a <see cref="IWriteStrategy"/>.
        /// </summary>
        /// <param name="fileType">A <see cref="IFileType"/> instance.</param>
        /// <returns>A <see cref="IWriteStrategy"/> instance that handles the fileType.</returns>
        Type GetWriteStrategy(IFileType fileType);

        /// <summary>
        /// Registers a new <see cref="IWriteStrategy"/> for a <see cref="IFileType"/>.
        /// </summary>
        /// <param name="fileType">The FileType</param>
        /// <param name="strategy">The WriteStrategy</param>
        void RegisterWriteStrategy(IFileType fileType, Type strategy);

        /// <summary>
        /// Deregisters the write strategy.
        /// </summary>
        /// <param name="fileType">Type of the file.</param>
        void DeregisterWriteStrategy(IFileType fileType);
    }
}
