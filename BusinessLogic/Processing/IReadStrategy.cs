﻿namespace DittoDir.BusinessLogic.Processing
{
    using DataAccess.FileSystem;

    /// <summary>
    /// Represents a read strategy
    /// </summary>
    public interface IReadStrategy
    {
        /// <summary>
        /// Reads Metadata from a <see cref="IAmFileSystemObject" />.
        /// </summary>
        /// <param name="source">The source.</param>
        /// <param name="options">The options.</param>
        /// <returns>
        /// An <see cref="IMetaData" /> instance representing the source.
        /// </returns>
        IMetaData Read(IAmFileSystemObject source, IStrategyOptions options);

        /// <summary>
        /// Gets the specific options.
        /// </summary>
        /// <returns>A new instance of the strategy options</returns>
        IStrategyOptions GetOptions();
    }
}
