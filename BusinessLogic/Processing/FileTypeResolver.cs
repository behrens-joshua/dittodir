﻿namespace DittoDir.BusinessLogic.Processing
{
    using DataAccess.FileSystem;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics.Contracts;
    using System.Linq;

    public class FileTypeResolver : IFileTypeResolver
    {
        /// <summary>
        /// The file types
        /// </summary>
        private readonly ICollection<IFileType> _fileTypes = new List<IFileType>();

        /// <summary>
        /// The longest magic number
        /// </summary>
        private int? _longestMagicNumber;

        /// <summary>
        /// Gets the longest magic number.
        /// </summary>
        private int LongestMagicNumber
        {
            get
            {
                // Find the longest magic number only when it was null.
                if (!_longestMagicNumber.HasValue)
                {
                    _longestMagicNumber = _fileTypes.SelectMany(p => p.MagicNumber.Select(pp => pp.Length)).Concat(new int[] { 0 }).Max();
                }

                return _longestMagicNumber.GetValueOrDefault();
            }
        }

        /// <summary>
        /// Resolves a <see cref="IAmFile"/> to a <see cref="IFileType"/>
        /// </summary>
        /// <param name="fileTypeDescriptor">Any <see cref="IAmFile"/> instance.</param>
        /// <returns>An instance of <see cref="IFileType"/>.</returns>
        public void Register(IFileType fileTypeDescriptor)
        {
            Contract.Requires<ArgumentNullException>(fileTypeDescriptor != null, nameof(fileTypeDescriptor));
            Contract.Requires<ArgumentException>(fileTypeDescriptor.MagicNumber == null || Contract.ForAll(fileTypeDescriptor.MagicNumber, (p) => (p?.Length ?? 0) > 0), nameof(fileTypeDescriptor));
            Contract.Requires<ArgumentException>(fileTypeDescriptor.FileExtension == null || Contract.ForAll(fileTypeDescriptor.FileExtension, (p) => (p?.Length ?? 0) > 0), nameof(fileTypeDescriptor));

            _fileTypes.Add(fileTypeDescriptor);
            _longestMagicNumber = null;
        }

        /// <summary>
        /// Deregisters the specified file type descriptor.
        /// </summary>
        /// <param name="fileTypeDescriptor">The file type descriptor.</param>
        public void Deregister(IFileType fileTypeDescriptor)
        {
            Contract.Requires<ArgumentNullException>(fileTypeDescriptor != null, nameof(fileTypeDescriptor));

            _fileTypes.Remove(fileTypeDescriptor);
            _longestMagicNumber = null;
        }

        /// <summary>
        /// Registers a new <see cref="IFileType"/>.<br />
        /// Every filetype must be registered first.
        /// </summary>
        /// <param name="obj">A <see cref="IAmFileSystemObject"/> instance.</param>
        public IFileType Resolve(IAmFileSystemObject obj)
        {
            Contract.Requires<ArgumentNullException>(obj != null, nameof(obj));
            IFileType fileType = null;

            if (obj is IAmDirectory)
            {
                fileType = _fileTypes.FirstOrDefault(p =>
                                        p.IsFolder &&
                                        (p.MagicNumber == null || p.MagicNumber.Count() == 0) &&
                                        (p.FileExtension == null || p.FileExtension.Count() == 0));
            }
            else
            {
                var file = (IAmFile)obj;
                // First try to find type by extension
                fileType = _fileTypes.FirstOrDefault(ft => ft.FileExtension.Contains(file.FileExtension));
                if (fileType == null)
                {
                    // If extension search failes, try by magic number
                    byte[] fileBytes = file.ReadBytes(LongestMagicNumber);

                    Contract.Assume(fileBytes != null);

                    fileType = _fileTypes.FirstOrDefault(p => p.MagicNumber.Any(pp => pp.SequenceEqual(fileBytes.Take(pp.Length))));
                }
            }
            
            return fileType;
        }
    }
}
