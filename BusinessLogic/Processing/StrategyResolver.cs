﻿namespace DittoDir.BusinessLogic.Processing
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics.Contracts;

    /// <summary>
    /// Default implementation of a strategy resolver.
    /// </summary>
    /// <seealso cref="IReadStrategyResolver" />
    /// <seealso cref="IWriteStrategyResolver" />
    public class StrategyResolver : IReadStrategyResolver, IWriteStrategyResolver
    {
        /// <summary>
        /// The read strategies
        /// </summary>
        private readonly IDictionary<IFileType, Type> _readStrategies = new Dictionary<IFileType, Type>();

        /// <summary>
        /// The write strategies
        /// </summary>
        private readonly IDictionary<IFileType, Type> _writeStrategies = new Dictionary<IFileType, Type>();

        /// <summary>
        /// The read strategy type for saving time resolving the type.
        /// </summary>
        private readonly Type _readStrategyType = typeof(IReadStrategy);

        /// <summary>
        /// The write strategy type for saving time resolving the type.
        /// </summary>
        private readonly Type _writeStrategyType = typeof(IWriteStrategy);

        /// <summary>
        /// Gets a <see cref="IReadStrategy" />.
        /// </summary>
        /// <param name="fileType">A <see cref="IFileType" /> instance.</param>
        /// <returns>
        /// A <see cref="IReadStrategy" /> instance that handles the fileType.
        /// </returns>
        public Type GetReadStrategy(IFileType fileType)
        {
            return _readStrategies[fileType];
        }

        /// <summary>
        /// Gets a <see cref="IWriteStrategy" />.
        /// </summary>
        /// <param name="fileType">A <see cref="IFileType" /> instance.</param>
        /// <returns>
        /// A <see cref="IWriteStrategy" /> instance that handles the fileType.
        /// </returns>
        public Type GetWriteStrategy(IFileType fileType)
        {
            return _writeStrategies[fileType];
        }

        /// <summary>
        /// Registers a new <see cref="IReadStrategy" /> for a <see cref="IFileType" />.
        /// </summary>
        /// <param name="fileType">The FileType</param>
        /// <param name="strategy">The ReadStrategy</param>
        /// <exception cref="ArgumentNullException">
        /// fileType;No filetype was provided.
        /// or
        /// strategy;No strategy type was provided.
        /// </exception>
        /// <exception cref="ArgumentException">
        /// Type is not derived from IReadStrategy.;strategy
        /// or
        /// Type has no default constructor.;strategy
        /// </exception>
        public void RegisterReadStrategy(IFileType fileType, Type strategy)
        {
            if(fileType == null)
            {
                throw new ArgumentNullException(nameof(fileType), "No filetype was provided.");
            }

            if(strategy == null)
            {
                throw new ArgumentNullException(nameof(strategy), "No strategy type was provided.");
            }

            if (strategy.GetConstructor(Type.EmptyTypes) != null)
            {
                throw new ArgumentException("Type has no default constructor.", nameof(strategy));
            }

            Contract.EndContractBlock();

            if (_readStrategyType.IsAssignableFrom(strategy))
            {
                throw new ArgumentException("Type is not derived from IReadStrategy.", nameof(strategy));
            }

            _readStrategies.Add(fileType, strategy);
        }

        /// <summary>
        /// Registers a new <see cref="IWriteStrategy" /> for a <see cref="IFileType" />.
        /// </summary>
        /// <param name="fileType">The FileType</param>
        /// <param name="strategy">The WriteStrategy</param>
        /// <exception cref="ArgumentNullException">
        /// fileType;No filetype was provided.
        /// or
        /// strategy;No strategy type was provided.
        /// </exception>
        /// <exception cref="ArgumentException">
        /// Type is not derived from IWriteStrategy.;strategy
        /// or
        /// Type has no default constructor.;strategy
        /// </exception>
        public void RegisterWriteStrategy(IFileType fileType, Type strategy)
        {
            if (fileType == null)
            {
                throw new ArgumentNullException(nameof(fileType), "No filetype was provided.");
            }

            if (strategy == null)
            {
                throw new ArgumentNullException(nameof(strategy), "No strategy type was provided.");
            }

            if (strategy.GetConstructor(Type.EmptyTypes) != null)
            {
                throw new ArgumentException("Type has no default constructor.", nameof(strategy));
            }

            Contract.EndContractBlock();

            if (_writeStrategyType.IsAssignableFrom(strategy))
            {
                throw new ArgumentException("Type is not derived from IWriteStrategy.", nameof(strategy));
            }

            _writeStrategies.Add(fileType, strategy);
        }

        /// <summary>
        /// Deregisters the read strategy.
        /// </summary>
        /// <param name="fileType">Type of the file.</param>
        public void DeregisterReadStrategy(IFileType fileType)
        {
            _readStrategies.Remove(fileType);
        }

        /// <summary>
        /// Deregisters the write strategy.
        /// </summary>
        /// <param name="fileType">Type of the file.</param>
        public void DeregisterWriteStrategy(IFileType fileType)
        {
            _writeStrategies.Remove(fileType);
        }
        
    }
}
