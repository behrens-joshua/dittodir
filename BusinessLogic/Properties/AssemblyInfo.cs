﻿using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("DittoDir BusinessLogic")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("")]
[assembly: AssemblyProduct("DittoDir")]
[assembly: AssemblyCopyright("Copyright © Thorsten Gerds, Joshua Behrens 2016")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

[assembly: ComVisible(false)]
[assembly: Guid("c4597140-ec1a-4c39-84c9-971e8f01e31f")]
[assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyFileVersion("1.0")]
